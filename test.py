#coding=utf-8


import responses
from unittest import TestCase
from sms_app import send_sms_code, ErrorCode, \
        SMS_SERVICE_URL, SMS_API_KEY

class SendSmsCodeTestCase(TestCase):
    def setUp(self):
        self.mobile = '12345678'
        self.code = '95687'
        self.sms_service_url = SMS_SERVICE_URL
        self.sms_api_key = SMS_API_KEY

    @responses.activate
    def test_http_404(self):
        bad_url = 'http://www.example.com/api/v3/send-code'
        responses.add(responses.POST, bad_url,
                json={'error': 'not found'}, status=404)
        self.assertNotEqual(bad_url, self.sms_service_url)
        result = send_sms_code(self.mobile, self.code, bad_url, self.sms_api_key)
        self.assertEqual(result, 'failed:%s' % ErrorCode.ERROR_CODE_HTTP_ERROR_404)

    @responses.activate
    def test_sms_api_unauthorized(self):
        responses.add(responses.POST, self.sms_service_url,
                json={'code': 6002}, status=200)
        result = send_sms_code(self.mobile, self.code, self.sms_service_url, 'wrong api key')
        self.assertEqual(result, 'failed:%s' % ErrorCode.ERROR_CODE_UNAUTHORIZED)

    @responses.activate
    def test_sms_api_success(self):
        responses.add(responses.POST, self.sms_service_url,
                json={'code': 6001}, status=200)
        result = send_sms_code(self.mobile, self.code, self.sms_service_url, self.sms_api_key)
        self.assertEqual(result, 'success')

