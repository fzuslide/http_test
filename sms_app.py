#coding=utf-8

import requests
from enum import Enum
import logging

SMS_SERVICE_URL = "http://www.example.com/api/v2/send-code"
SMS_API_KEY = "TESTAPIKEY"

class ErrorCode(Enum):
    # Http Error
    ERROR_CODE_HTTP_ERROR_404 = 10404

    # Sms API Error
    ERROR_CODE_UNAUTHORIZED = 20001
    ERROR_CODE_INVALID_MOBILE = 20002
    ERROR_CODE_MOBILE_IN_BLACKLIST = 20003
    ERROR_CODE_NO_QUOTA = 20004
    ERROR_CODE_SMS_API_UNKNOWN = 20005

    # Ext Error
    ERROR_CODE_UNKNOWN = 20006


class SmsAPICode(Enum):
    SUCCESS = 6001
    UNAUTH = 6002
    BLACKLIST = 6003
    NO_QUOTA = 6004

    @classmethod
    def get_error_code(cls, sms_code):
        _error_map = {
                cls.UNAUTH.value: ErrorCode.ERROR_CODE_UNAUTHORIZED,
                cls.BLACKLIST.value: ErrorCode.ERROR_CODE_MOBILE_IN_BLACKLIST,
                cls.NO_QUOTA.value: ErrorCode.ERROR_CODE_NO_QUOTA,
                }
        return _error_map.get(sms_code, ErrorCode.ERROR_CODE_SMS_API_UNKNOWN)



def send_sms_code(mobile, code, sms_service_url='', api_key=''):
    """
    Send Sms code by http service
    """
    data = {'mobile': mobile, 'code': code, 'apiKey': api_key}
    try:
        resp = requests.post(sms_service_url, data=data)
        resp.raise_for_status()
        resp_json = resp.json()
        if resp_json['code'] == SmsAPICode.SUCCESS.value:
            return 'success'
        return 'failed:%s' % SmsAPICode.get_error_code(resp_json['code'])
    except requests.exceptions.HTTPError as err:
        logging.error(err)
        return 'failed:%s' % ErrorCode.ERROR_CODE_HTTP_ERROR_404
    except Exception as err:
        logging.error(err)
        return 'failed:%s' % ErrorCode.ERROR_CODE_SMS_API_UNKNOWN

    return 'failed:%s' % ErrorCode.ERROR_CODE_UNKNOWN


if __name__ == '__main__':
    result = send_sms_code('12345678', '568988')
    print(result)

